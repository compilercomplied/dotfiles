

alias urxvt=rxvt-unicode
alias ncm='ncmpcpp -c ~/.ncmpcpp/bindings -c ~/.ncmpcpp/config'

# tmux
alias tmux='tmux -f ~/.config/tmux/tmux.conf'
alias tmuxk='tmux kill-session -t'
alias tmuxa='tmux attach-session -t'
alias tmuxs='tmux new -s '

# newsbeuter
alias newsbeuter='newsbeuter -C ~/.config/newsbeuter/config'
alias rss-cs='newsbeuter -C ~/.config/newsbeuter/config -u ~/.config/newsbeuter/feeds/tech'
alias rss-read='newsbeuter -C ~/.config/newsbeuter/config -u ~/.config/newsbeuter/feeds/cultural'
alias rss-cook='newsbeuter -C ~/.config/newsbeuter/config -u ~/.config/newsbeuter/feeds/cooking'
alias rss-comic='newsbeuter -C ~/.config/newsbeuter/config -u ~/.config/newsbeuter/feeds/comics'
